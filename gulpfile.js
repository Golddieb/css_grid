'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");
let cleanCSS = require('gulp-clean-css');

gulp.task('sass', function () {
  var min = gulp.src('./src/**/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(cleanCSS())
	.pipe(rename({
	  basename: "grid.minified"
	  // dirname: "main/text/ciao",
	  // prefix: "bonjour-",
	  // suffix: "-hola",
	  // extname: ".md"
	}))
	.pipe(gulp.dest('./dist/'));

  var source = gulp.src('./src/**/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(rename({
	  basename: "grid.src"
	  // dirname: "main/text/ciao",
	  // prefix: "bonjour-",
	  // suffix: "-hola",
	  // extname: ".md"
	}))
	.pipe(gulp.dest('./dist/'));

  return min,source;
});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});


gulp.task('minify', function() {
  return gulp.src('src/*.css')
	.pipe(cleanCSS())
	.pipe(gulp.dest('dist'));
});